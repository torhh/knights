/*
 * Decode the ROSTER.DAT file from Knights of the Sky.
 * ROSTER.DAT contains all the characters a player can choose from (or erase to create her/his own)
 * There is 10 characters in total, each occupies 3528 bytes.
 *
 * This is the only file which is subjected to change duing the game. So all ACES data are in here
 * as well. But I don't know how this is mapped against the rest of the game yet.
 *
 * We might call this the save game file....
 *
 *
 * 0. Corporal, 1. 2nd Lt., 2. Lt., 3. Captain, 4. Major, 5. Lt. Col, 6. Colonel
*/


#include <stdio.h>
#include <stdlib.h>

#define PSize	3528            /* One character occupies this amount of the file */
#define PPlayer 10              /* There are 10 character slots available. No more, no less */

/* Must be 3528 bytes */
struct Player {
	char player[30];            /* character name */
	unsigned short u1;          /* Unkown - might just be a 2 byte NULL terminator */
	unsigned short rank;        /* 0h Captain, 1h 2nd Lt, 2h Lt, 3h Captain, 4h Major */
	unsigned short nationality; /* 0x06 = ENG, 0x0A = FRENCH */
	unsigned short month;       /* current month in game */
	unsigned short day;         /* current day in game */
	unsigned short year;        /* current year in game */
	unsigned short u4;          /* Unknown */
	unsigned short status;      /* 1h Active, 3h Retired ?? Offset: 0x2C*/
	unsigned short u7;          /* 5h Active, bh Retired ???? */
	unsigned short u8;          /* 1h Active, bh Retired */
	unsigned short u9;
	unsigned short kills;
	unsigned short missions;
	unsigned short u6;          /* Unknown */
	unsigned short resurrections;	/* Offset: 0x3A */
	unsigned short u10;         /* Unknown: Always 3h */
	unsigned short u11;
	char u2[3464];              /* Unknown */	
	
};

void printPlayer(struct Player *);

int main(int argc, char *argv[])
{
    struct Player *p = (struct Player*) malloc(sizeof(struct Player));

	// Make sure we read the correct amount of data
	if (sizeof(struct Player) != PSize)
	{
		printf("Struct out of Alignment: %ld bytes\n", (PSize - sizeof(struct Player)));
        free(p);
		return 0;
	}

	FILE *fp;
	char *fname = "ROSTER.DAT";
	fp = fopen(fname, "r");

	int i;
	for (i = 0; i < PPlayer; i++)
	{
		fread(p, sizeof(struct Player), 1, fp);
		printPlayer(p);
	}

    fclose(fp);
    free(p);
}

void printPlayer(struct Player *p)
{
	printf("Name         : %s\n", p->player);
	printf("Rank         : %d\n", p->rank);
	printf("Status       : %d\n", p->status);
	printf("Date         : %d %d, %d\n", p->month, p->day, p->year);
	printf("Nationality  : %d  ( 06 = ENG, 10 = FRA )\n", p->nationality);
	printf("Kills        : %d\n", p->kills);
	printf("Missions     : %d\n", p->missions);
	printf("Resurrections: %d\n", p->resurrections);
//	printf("Unknown u11: %d\n", p->u11);
	printf("\n");
}
