#include <stdio.h>

/*
 * A small program to examin the header of a DOS executable.
 *
*/

struct EXE {
	unsigned short signature;               /* 0x5a4d */
	unsigned short bytes_in_last_block;
	unsigned short blocks_in_file;          /* One block is 512 bytes    */
	unsigned short num_relocs;
	unsigned short header_paragraphs;       /* One paragraph is 16 bytes */
	unsigned short min_extra_paragraphs;
	unsigned short max_extra_paragraphs;
	unsigned short ss;
	unsigned short sp;
	unsigned short checksum;
	unsigned short ip;
	unsigned short cs;
	unsigned short reloc_table_offset;
	unsigned short overlay_number;
};

void print_header(struct EXE *, char *);
int is_valid_header(struct EXE *);

int main(int argc, char *argv[])
{
	int ret;
	char *file;
	FILE *fp;
	struct EXE header;

	if (argc != 2)
	{
		printf("Usage: %s <DOS executable>\n", argv[0]);
		return 0;
	}

	file = argv[1];

	fp = fopen(file, "r");
	if (!fp) {
		printf("File '%s' not found\n", file);
		return 0;
	}

	ret = fread(&header, sizeof(struct EXE), 1, fp);
	if (!is_valid_header(&header)) {
		printf("This is not a valid DOS executable file\n");
		return 0;
	}

	print_header(&header, file);
    fclose(fp);
}

int is_valid_header(struct EXE *h)
{
	if (h->signature == 0x5a4d)
		return 1;
	return 0;
}

void print_header(struct EXE *h, char *f)
{
	printf("\n\n=== DOS EXECUTABLE HEADER VIEWER ===\n\tFile: %s\n\n", f);
	printf("Signature           : 0x%x\n", h->signature);
	printf("Bytes in last block : %d\n", h->bytes_in_last_block);
	printf("Blocks in file      : %d\n", h->blocks_in_file);
	printf("Num relocs          : %d\n", h->num_relocs);
	printf("Header paragraphs   : %d\n", h->header_paragraphs);
	printf("Min extra paragraphs: %d\n", h->min_extra_paragraphs);
	printf("Max extra paragraphs: %d\n", h->max_extra_paragraphs);
	printf("Checksum            : %d\n", h->checksum);
	printf("Reloc table offset  : 0x%04x\n", h->reloc_table_offset);
	printf("Overlay number      : %d\n", h->overlay_number);
	printf("\nss : 0x%04x\nsp : 0x%04x\nip : 0x%04x\ncs : 0x%04x\n\n", h->ss, h->sp, h->ip, h->cs);
	printf("Exe data start      : 0x%04lx\n", h->header_paragraphs * 16L);
	
	long extra_data_start = h->blocks_in_file * 512L;
	if (h->bytes_in_last_block)
		extra_data_start -= (512 - h->bytes_in_last_block);
	printf("Extra data start    : 0x%04lx <-- most likely EOF\n", extra_data_start);
	printf("\n"); 

}
