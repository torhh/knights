/*
 * Run on MIS1.DAT, MIS2.DAT or MIS3.DAT
 * The contains the same base name, but they do differ on a binary level.
 *
 * Names begin with a offset of 0x1000, and there is a gap between the last
 * entry and the end of file with 92 bytes.
 *
 * compile with: gcc viewdat.c -o viewdat
 * usage: ./viewdat MIS1.DAT (or MIS2 og MIS3)
*/

#include <stdio.h>
#include <stdlib.h>

#define OFFSET 0x1000

struct base {
    unsigned short unknown;
    char name[32];
};

void printbase(struct base*);

int main(int argc, char *argv[])
{
    FILE *fp;
    char *filename = NULL;
    struct base *bp = (struct base*) malloc(sizeof(struct base));
    int num, last_offset, i;

    if (argc != 2)
        return 0;
    filename = argv[1];

    fp = fopen(filename, "r");
    if (!fp)
        return 0;

    fseek(fp, OFFSET, SEEK_SET);
    fread(&num, sizeof(short int), 1, fp);

    for (i = 0; i < num; ++i)
    {
        fread(bp, sizeof(struct base), 1, fp);
        printbase(bp);
    }

    last_offset = ftell(fp);
    fseek(fp, 0, SEEK_END);
    printf("\nFound %d entries. Gap between last entry and end of file "
           "is %ld bytes\n\n", num, ftell(fp)-last_offset);
    free(bp);
    fclose(fp);
}

void  printbase(struct base *bp)
{
    printf("Base: %-30s Unknown: 0x%04x (%d)\n", bp->name, bp->unknown, bp->unknown);
}
