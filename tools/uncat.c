/*
 * A small program to unpack .cat files from Knights of the Sky.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* struct must be 24 bytes */
typedef struct {
    char        name[12];   /* Filename with a DOS limit on 8.3 name structure  */
    unsigned    u1;         /* Unknown                                          */
    unsigned    size;       /* File size                                        */
    unsigned    offset;     /* Offset (from start of file)                      */
} FileInfo_t;

enum {
    VIEW,
    UNPACK
};

static int nfilesoverwritten;

void print_files(FileInfo_t *);
int unpack(FileInfo_t *, FILE *);
void usage(char *s);

int main(int argc, char *argv[])
{
    FILE *fp;
    FileInfo_t *fi = malloc(sizeof(FileInfo_t));
    char *filename;
    int i;

    int nfiles = 0;
    int param = VIEW;

    if (argc == 3 && !strcmp(argv[1], "--unpack")) {
        param = UNPACK;
        filename = argv[2];
    }
    else if (argc == 2)
        filename = argv[1];
    else
        usage(argv[0]);

    fp = fopen(filename, "r");
    if (!fp)
        return -1;

    fread(&nfiles, 2, 1, fp);

    printf("\nFound %d files\n\n", nfiles);

    for (i = 0; i < nfiles; i++) {
        fread(fi, sizeof(FileInfo_t), 1, fp);
        print_files(fi);
        if (param == UNPACK)
            unpack(fi, fp);
    }
    if (param == UNPACK)
    {
        printf("\n%d files extracted", i);
        if (nfilesoverwritten)
            printf(" - %d files overwritten\n", nfilesoverwritten);
    }
    printf("\n");

    fclose(fp);
    free(fi);
    return 0;
}

void print_files(FileInfo_t *fi)
{
    char name[13];
    sprintf(name, "%.12s", fi->name);
    printf("%-12s\tOffset: 0x%08x\tSize: %6d bytes\n", name, fi->offset, fi->size);
}

int unpack(FileInfo_t *fi, FILE *source)
{
    FILE *dest;
    int  pos = ftell(source);       /* Save file position for return */
    int  i;
    char ch;

    char name[13];
    sprintf(name, "%.12s", fi->name);

    if (!access(name, F_OK))
        nfilesoverwritten++;

    dest = fopen(name, "w");        /* will overwrite files with same name */
    if (!dest)
        return -1;

    fseek(source, fi->offset, SEEK_SET);
    
    for (i = 0; i < fi->size; i++)
    {
        ch = getc(source);
        putc(ch, dest);
    }

    fclose(dest);
    fseek(source, pos, SEEK_SET);   /* Restore file position    */
    return 0;
}

void usage(char *f)
{
    printf("%s [--unpack] <filename>\n", f);
    printf("Use --unpack parameter to unpack.\n");
    printf("Warning: Files will be unpack in current directory!\n\n");
}
